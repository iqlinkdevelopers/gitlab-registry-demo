FROM python:3.7-alpine3.10
RUN mkdir -p /app
WORKDIR /app
COPY ./requirements.txt .
RUN pip install -r requirements.txt
COPY ./app ./
EXPOSE 80
CMD ["python", "-u", "server.py"]